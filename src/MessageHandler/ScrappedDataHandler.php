<?php
namespace App\MessageHandler;

use App\Entity\Property;
use App\Message\ScrappedPage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ScrappedDataHandler implements MessageHandlerInterface
{
    protected EntityManagerInterface $em;
    protected LoggerInterface $logger;
    protected ValidatorInterface $validator;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->validator = $validator;
    }

    public function __invoke(ScrappedPage $message)
    {
        $this->logger->info('Got scrapped data in async worker. Congrats!');

        $contentMap = [];
        // I know it ain't beautiful but lets fish out the uuids to query all of them at once
        foreach ($message->getContent() as $propertyDetails) {
            $this->logger->debug($propertyDetails['uuid']);
            $contentMap[$propertyDetails['uuid']] = $propertyDetails;
        }

        // Lets get all existing ones, so we update them ,the rest will have to be created.
        $existingProperties = $this->em->getRepository(Property::class)
            ->findBy([
                'uuid' => array_keys($contentMap),
            ]);

        $existingPropertiesMap = [];
        foreach ($existingProperties as $dbRecord) {
            $existingPropertiesMap[$dbRecord->getUuid()] = $dbRecord;
        }

        $this->actionResolverAndFlush($contentMap, $existingPropertiesMap);

        return null;
    }

    /*
     * I built two maps -> one of the new incoming data, other with the existing records by uuid.
     * If uuid exists in the DB i will update it to what's new, if not present -> create a new record
     * */
    protected function actionResolverAndFlush(array $contentMap, array $existingPropertiesMap): void
    {
        foreach ($contentMap as $uuid => $content) {
            $property = new Property();

            // If property exists we will only update the values
            if (array_key_exists($uuid, $existingPropertiesMap)) {
                $property = $existingPropertiesMap[$uuid];
                $this->logger->debug('Updating record for uuid : ' . $uuid);
            } else {
                // It doesn't exist -> create new record, that doesn't exist yet in the db.
                $property->setUuid($content['uuid']);
                $this->logger->debug('Assigned new uuid : ' . $content['uuid']);
            }

            $this->logger->debug('Processing async the property uuid: ' . $property->getUuid());

            $property->setCountry($content['country']);
            $property->setCounty($content['county']);
            $property->setDescription($content['description']);
            $property->setDisplayableAddress($content['address']);
            $property->setPostcode($this->extractPostcode($content['address']));
            $property->setType($content['type']);
            $property->setImage($content['image_full']);
            $property->setThumbnail($content['image_thumbnail']);
            $property->setLatitude($content['latitude']);
            $property->setLonitude($content['longitude']);
            $property->setNoBathrooms($content['num_bathrooms']);
            $property->setNoBedrooms($content['num_bedrooms']);
            $property->setPrice($content['price']);
            $property->setPropertyType($content['property_type_id']);
            $property->setTown($content['town']);

            $errors = $this->validator->validate($property);
            if (count($errors) > 0) {
                $this->logger->error($property->getUuid() . ' failed validation.');
            }
            // Signalise to the doctrine to mark to deal with it at the end of iteration below (lets avoid strain on doctrine,
            // it's not the most efficient piece of software.
            $this->em->persist($property);
        }
//        Save only once, since writing is expensive.
        $this->em->flush();
    }

    /*
     *  This isn not a beautiful piece but since ive got to somehow extract the postcode...
     * */
    protected function extractPostcode(string $address): string
    {
        $matches = [];
        $this->logger->debug('Received address: ' . $address);
        preg_match('/^(.*?) /', $address, $matches);
        $this->logger->debug('Match: ' . $matches[1]);

        return $matches[1];
    }
}
