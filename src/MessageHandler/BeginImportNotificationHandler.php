<?php
namespace App\MessageHandler;

use App\Message\BeginImportNotification;
use App\Message\ScrappedPage;
use App\Service\HttpClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class BeginImportNotificationHandler implements MessageHandlerInterface
{
    protected MessageBusInterface $bus;
    protected LoggerInterface $logger;
    protected HttpClient $httpClient;

    public function __construct(MessageBusInterface $bus, LoggerInterface $logger, HttpClient $httpClient)
    {
        $this->bus = $bus;
        $this->logger = $logger;
        $this->httpClient = $httpClient;
    }

    public function __invoke(BeginImportNotification $message)
    {
        // This is handler in the cli container, so it doesnt take the precious resources of the fpm container (it should handle the tcp traffic, not migrations!)
        $this->logger->debug($message->getContent());
        $this->logger->info('Lets start the async migration!');

        // By knowing last page i know how many pages to crawl through, and yes it could be optimised to use one request less.
        $pageCount = $this->getInfoPage()['last_page'];

        for ($iterationNumber = 1; $iterationNumber <= $pageCount; ++$iterationNumber) {
            $page = $this->httpClient->getPage($iterationNumber)['data'];
            $this->bus->dispatch(new ScrappedPage($page));
        }
    }

    /* I will use this to get count of all the pages to iterate them and put in the bus later on*/
    protected function getInfoPage(): array
    {
        return $this->httpClient->getPage(1, 100);
    }
}
