<?php
namespace App\MessageHandler;

use App\Message\AvatarCreator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AvatarCreatorHandler implements MessageHandlerInterface
{
    protected LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function __invoke(AvatarCreator $message)
    {
        $this->logger->info('Lets create the avatar for the uploaded image: php bin/console liip:imagine:cache:resolve media/' . $message->getContent());

        $output = [];
        exec('php bin/console liip:imagine:cache:resolve media/' . $message->getContent(), $output);

        $this->logger->info(implode('|', $output));
    }
}
