<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends AbstractController
{
    protected function getJsonBody(Request $request): array
    {
        return json_decode((string) $request->getContent(), true, 512, JSON_THROW_ON_ERROR);
    }
}
