<?php
namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Property;
use App\Form\PropertyType;
use App\Form\SearchPropertyType;
use App\Repository\PropertyRepository;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class PropertyController extends BaseController
{
    /**
     * @Route("/", name="property_index", methods={"GET"})
     */
    public function index(PropertyRepository $propertyRepository): Response
    {
        return $this->render('property/index.html.twig', [
            'property' => $propertyRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="property_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $property = new Property();
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form->get('image')->getData();

            // this condition is needed because the 'image' field is not required
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $property->setImage($imageFileName);
            }

            $property->setUuid(Uuid::v6());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($property);
            $entityManager->flush();

            return $this->redirectToRoute('property_index');
        }

        return $this->render('property/new.html.twig', [
            'property' => $property,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{uuid}", name="property_show", methods={"GET"}, requirements={"uuid"="%regex.uuid%"})
     */
    public function show(Property $property): Response
    {
        return $this->render('property/show.html.twig', [
            'property' => $property,
        ]);
    }

    /**
     * @Route("/search", name="property_search", methods={"GET","POST"})
     */
    public function search(Request $request, PropertyRepository $repository): Response
    {
        $form = $this->createForm(SearchPropertyType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $repository->searchPropertiesByValues($form->getData());

            return $this->render('property/index.html.twig', [
                'property' => $data, // this should be array of properties matching the expected
            ]);
        }

        return $this->render('property/search.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{uuid}/edit", name="property_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Property $property, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form->get('image')->getData();

            // this condition is needed because the 'image' field is not required
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $property->setImage($imageFileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('property_index');
        }

        return $this->render('property/edit.html.twig', [
            'property' => $property,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{uuid}", name="property_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Property $property): Response
    {
        if ($this->isCsrfTokenValid('delete' . $property->getUuid(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($property);
            $entityManager->flush();
        }

        return $this->redirectToRoute('property_index');
    }
}
