<?php
namespace App\Controller;

use App\Message\BeginImportNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class RootController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     * @Route("/version", name="version", methods={"GET"})
     */
    public function home(string $projectDir, string $projectName): JsonResponse
    {
        $version = @file_get_contents($projectDir . '/VERSION');

        return new JsonResponse([
            'name' => $projectName,
            'version' => $version ? trim($version) : 'unknown',
        ]);
    }

    /**
     * @Route("/notify", name="notify", methods={"GET"})
     */
    public function notify(MessageBusInterface $bus): JsonResponse
    {
        // will caused the handler to be called, async messages are handled by the worker.
        $bus->dispatch(new BeginImportNotification('The content here is not important, it just triggers the start action.'));

        return new JsonResponse([
            'congrats' => 'you just triggered an async event to begin the DB migration',
        ]);
    }
}
