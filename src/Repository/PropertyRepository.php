<?php
namespace App\Repository;

use App\Entity\Property;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Property::class);
    }

    /*
     * I am not sure yet if i will need those, but i might
     * Thinking about it later i could link the type of search using the properties below to search exact or CONTAINS etc.
     */
    protected static array $searchableFields = [
        'county',
        'country',
        'town',
        'postcode',
        'no_bedrooms',
        'no_bathrooms',
        'property_type',
        'type',
    ];

    /*
     * This is a poor search, only by exact match, but the spec doesn't say it has to be more flexible. It has been a long day and I had enough.
     * */
    public function searchPropertiesByValues(array $fields): ?array
    {
        $fields = $this->extractNotNullFields($fields);

        if ($fields) {
            $query = $this->createQueryBuilder('p');

            foreach ($fields as $key => $value) {
                $query->andWhere('p.' . $key . ' = :val') // this is a basic search ok? we could search for fields using %% but overkill for an exercise.
                     ->setParameter('val', $value);
            }

            return $query->getQuery()
                 ->getResult();
        }

        return null;
    }

    protected function extractNotNullFields(array $fields): array
    {
        return array_filter($fields, static function ($value) {
            return !is_null($value);
        });
    }

    // /**
    //  * @return Property[] Returns an array of Property objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Property
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
