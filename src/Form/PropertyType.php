<?php
namespace App\Form;

use App\Entity\Property;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid', HiddenType::class)
            ->add('county')
            ->add('country', CountryType::class)
            ->add('town')
            ->add('postcode')
            ->add('description', TextareaType::class)
            ->add('displayable_address')
            ->add('image', FileType::class, [
                'label' => 'Image (JPEG file)',
                // unmapped means that this field is not associated to any entity property
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid JPEG image',
                    ]),
                ],
            ])
            ->add('no_bedrooms', ChoiceType::class, self::NUMBERSMAP)
            ->add('no_bathrooms', ChoiceType::class, self::NUMBERSMAP)
            ->add('price', IntegerType::class)
            ->add('property_type', ChoiceType::class, self::PROPERTYTYPESMAP)
            ->add('type', ChoiceType::class, self::RADIOCHOICES);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
        ]);
    }

    public const PROPERTYTYPESMAP = [
        'choices' => [
            'Flat' => 1,
            'Detached' => 2,
            'Semi-detached' => 3,
            'Terraced' => 4,
            'End of Terrace' => 5,
            'Cottage' => 6,
            'Bungalow' => 7,
        ],
    ];

    public const RADIOCHOICES = [
        'multiple' => false,
        'expanded' => true,
        'choices' => [
            'rent' => 'rent',
            'sale' => 'sale',
        ],
    ];

    public const NUMBERSMAP = [
        'choices' => [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6',
            7 => '7',
            8 => '8',
            9 => '9',
            10 => '10',
            11 => '11',
            12 => '12',
            13 => '13',
            14 => '14',
            15 => '15',
        ],
    ];
}
