<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchPropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('county', null, ['required' => false])
            ->add('country', CountryType::class, ['required' => false])
            ->add('town', null, ['required' => false])
            ->add('postcode', null, ['required' => false])
            ->add('no_bedrooms', ChoiceType::class, self::NUMBERSMAP)
            ->add('no_bathrooms', ChoiceType::class, self::NUMBERSMAP)
            ->add('property_type', ChoiceType::class, self::PROPERTYTYPESMAP)
            ->add('type', ChoiceType::class, self::RADIOCHOICES);
    }

    public const PROPERTYTYPESMAP = [
        'required' => false,
        'choices' => [
            'Flat' => 1,
            'Detached' => 2,
            'Semi-detached' => 3,
            'Terraced' => 4,
            'End of Terrace' => 5,
            'Cottage' => 6,
            'Bungalow' => 7,
        ],
    ];
    public const RADIOCHOICES = [
        'required' => false,
        'multiple' => false,
        'expanded' => true,
        'choices' => [
            'rent' => 'rent',
            'sale' => 'sale',
        ],
    ];

    public const NUMBERSMAP = [
        'required' => false,
        'choices' => [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6',
            7 => '7',
            8 => '8',
            9 => '9',
            10 => '10',
            11 => '11',
            12 => '12',
            13 => '13',
            14 => '14',
            15 => '15',
        ],
    ];
}
