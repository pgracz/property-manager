<?php
namespace App\Entity;

use App\Repository\PropertyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 */
class Property
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $town;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=65535)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $displayable_address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @ORM\Column(type="integer")
     */
    private $no_bedrooms;

    /**
     * @ORM\Column(type="integer")
     */
    private $no_bathrooms;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $property_type;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lonitude;

//    /**
//     * @ORM\Column(type="string", nullable=true)
//     */
//    private $fullDetailsUrl;

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDisplayableAddress(): ?string
    {
        return $this->displayable_address;
    }

    public function setDisplayableAddress(string $displayable_address): self
    {
        $this->displayable_address = $displayable_address;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(?string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getNoBedrooms(): ?int
    {
        return $this->no_bedrooms;
    }

    public function setNoBedrooms(int $no_bedrooms): self
    {
        $this->no_bedrooms = $no_bedrooms;

        return $this;
    }

    public function getNoBathrooms(): ?int
    {
        return $this->no_bathrooms;
    }

    public function setNoBathrooms(int $no_bathrooms): self
    {
        $this->no_bathrooms = $no_bathrooms;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPropertyType(): ?string
    {
        return $this->property_type;
    }

    public function setPropertyType(string $property_type): self
    {
        $this->property_type = $property_type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLonitude(): ?string
    {
        return $this->lonitude;
    }

    public function setLonitude(string $lonitude): void
    {
        $this->lonitude = $lonitude;
    }

//    The task specifies full detail url but i do not know what this applies to. the sample DB doesnt contain full details url to map here
//    public function getFullDetailsUrl(): ?string
//    {
//        return $this->fullDetailsUrl;
//    }
//
//    public function setFullDetailsUrl(string $fullDetailsUrl): void
//    {
//        $this->fullDetailsUrl = $fullDetailsUrl;
//    }
}
