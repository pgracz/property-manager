<?php
namespace App\Service;

use App\Service\Interfaces\HttpAdaptorInterface;
use Exception;
use Symfony\Component\HttpClient\HttpClient as SymfonyHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HttpClient implements HttpAdaptorInterface
{
    /** @var HttpClientInterface $client */
    protected HttpClientInterface $client;
    /** @var string $url */
    protected string $url;
    /** @var string $apiKey */
    protected string $apiKey;

    /**
     * HttpClient constructor.
     */
    public function __construct(string $url, string $apiKey)
    {
        $this->url = $url;
        $this->apiKey = $apiKey;
        $this->client = SymfonyHttpClient::create([
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws \JsonException
     */
    public function getRequest(array $params): array
    {
        try {
            $response = $this->client->request('GET', $this->url, $params);
            $response = $response->getContent();
        } catch (\Exception $exception) {
            throw new Exception('GET request to ' . $this->url . ' failed: ' . $exception->getMessage());
        }

        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    public function getPage(int $pageNumber = 1, int $pageSize = 100): array
    {
        $params = [
            'query' => [
                'api_key' => $this->apiKey,
                'page[number]' => $pageNumber,
                'page[size]' => $pageSize,
            ],
        ];

        return $this->getRequest($params);
    }
}
