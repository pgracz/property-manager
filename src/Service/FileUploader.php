<?php
namespace App\Service;

use App\Message\AvatarCreator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private string $targetDirectory;
    private SluggerInterface $slugger;
    private LoggerInterface $logger;
    private MessageBusInterface $bus;

    public function __construct(string $targetDirectory, SluggerInterface $slugger, LoggerInterface $logger, MessageBusInterface $bus)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
        $this->logger = $logger;
        $this->bus = $bus;
    }

    public function upload(UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename . '-' . uniqid('', false) . '.' . $file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);

            // async generate the avatar cache while uploading the image
            // if you want more resizing/ reshaping this would be the place for it. Lookup the config/packages/liip_imagine.yaml for bundle setup.
            $this->bus->dispatch(new AvatarCreator($fileName)); // This is an async command!
        } catch (\Throwable $e) {
            $this->logger->error($e->getMessage());
        }

        return $fileName;
    }

    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }
}
