<?php
namespace App\Service\Interfaces;

interface HttpAdaptorInterface
{
    public function getRequest(array $params): array;
}
