#!/bin/sh
set -e

composer install;

/usr/bin/wait-for-it database:3306

bin/console doctrine:migrations:migrate;

echo "Starting up ${*}..."
exec $@
