This is a dev project (proof of concept).

It lacks unit test and integration tests (My time is not unlimited :) i can write them if needed)
To run the project is requires docker-compose.
The first start will migrate the db the wait-for script isn't idea, so the worker may print a few errors (they're avoidable with more fine polish, the migrator should be an image on its own and signalise the end of migration process).

To run the project:
``` docker-compose -f ./docker/dev.yaml up --build``` 
The above will no only build the nginx, php-fpm, php-cli and mysql containers and do the network wiring.
Project utilizes message queues. Due to time constraints it isnt my belowed amqp, but database support will do for now.

Once build on the local machine go to localhost/admin to get index for the properties.
To trigger DB migration I did not plug in a crontastic container of an additional supervisor. Since it's a proof of concept you can trigger the
migration by visiting localhost/notify (GET). This will trigger an async migration using cli worker. Follow docker logs for details.
The task specifies to add full details url, but I do not know if I should generate (what for if the CRUD allow to get them by uuid) them or whatnot since this data isn't available in the source.

The crud options for admin space are under admin prefix (ofc :).

The image uploads are simple (use built-in symfony components). Full size jpegs are landing in the public/media directory. 
The File upload service also generates temp avatars (or thumbnails, its configurable). i didn't add as perm since images are disposable in this proof of concept.

For detail review the code. Most of it is up to php7.4 standard. If you have any quesions, feel free to let me know.

When running docker-compose the available routes are to be found in annotations within:

```
src/Controller/Admin/PropertyController.php
src/Controller/RootController.php
```
