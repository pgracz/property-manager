FROM php:7.4-fpm-alpine

# This dockerfile is not prod optimised. to do so it would pre-build composer and improve autoload.

MAINTAINER Peter Gracz <gracz.peter@gmail.com.com>
RUN curl https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh > /usr/bin/wait-for-it
RUN chmod +x /usr/bin/wait-for-it

# Setup GD extension
RUN apk add --no-cache \
      freetype \
      libjpeg-turbo \
      libpng \
      freetype-dev \
      libjpeg-turbo-dev \
      libpng-dev \
    && docker-php-ext-configure gd \
      --with-freetype=/usr/include/ \
      --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-enable gd \
    && apk del --no-cache \
      freetype-dev \
      libjpeg-turbo-dev \
      libpng-dev \
    && rm -rf /tmp/*

RUN apk --update --no-cache add git bash
RUN docker-php-ext-install pdo_mysql fileinfo
RUN docker-php-ext-enable fileinfo
COPY --from=composer /usr/bin/composer /usr/bin/composer
WORKDIR /var/www

CMD composer install; /bin/bash /usr/bin/wait-for-it database:3306 -- bin/console doctrine:migrations:migrate;  php-fpm
EXPOSE 9000
